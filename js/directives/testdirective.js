define(['./module'], function (directives) {
    'use strict';
   directives.directive('testdirective', [function () {
        return  {
            restrict : 'E',
            templateUrl: '/partials/auth-form.html'
        }
    }]);
})