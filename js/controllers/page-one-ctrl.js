define(['./module'], function (controllers) {
    'use strict';
    controllers.controller('PageOneCtrl', ['$rootScope','$scope', function ($rootScope,$scope) {
        $scope.title = "Первая страница";
    }]);
})