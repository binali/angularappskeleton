define(['./module'], function (controllers) {
    'use strict';
    controllers.controller('PageTwoCtrl', ['$rootScope','$scope', function ($rootScope,$scope) {
        $scope.title = "Вторая страница";
    }]);
})