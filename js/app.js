/**
 * loads sub modules and wraps them up into the main module
 * this should be used for top-level module definitions only
 */
define([
    'angular',
    'angular-route',
    'angular-cookies',
    'angular-resource',
    'angular-strap',
    'angular-animate',
    './services/index',
    './controllers/index',
    './directives/index',
  //  './filters/index'
], function (angular) {
    'use strict';

    return angular.module('app', [
        'app.controllers',
        'app.directives',
  //      'app.filters',
  //       'app.services',
        'ngRoute',
        'ngCookies',
        'ngResource',
        'mgcrea.ngStrap',
        'ngAnimate'
    ]);
});
