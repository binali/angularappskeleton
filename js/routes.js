/**
 * Defines the main routes in the application.
 * The routes you see here will be anchors '#/' unless specifically configured otherwise.
 */

define(['./app'], function (app) {
    'use strict';
    return app.config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/', {
            templateUrl: '/partials/main.html',
            controller: 'MainCtrl'
        });
        $routeProvider.when('/page-one',{
            templateUrl: '/partials/main.html',
            controller: 'PageOneCtrl'
        });
        $routeProvider.when('/page-one',{
            templateUrl: '/partials/page-one.html',
            controller: 'PageOneCtrl'
        });
        $routeProvider.when('/page-two',{
            templateUrl: '/partials/page-two.html',
            controller: 'PageTwoCtrl'
        });
        $routeProvider.otherwise({
            redirectTo: '/'
        });
    }]);
});
